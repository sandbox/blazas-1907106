<?php

/**
 * @file
 * Ubercart Heureka administration menu items.
 */

/**
 * Form builder for Heureka settings form.
 *
 * @ingroup forms
 */
function uc_heureka_settings_form() {
  $form['wrapper'] = array('#type' => 'fieldset', '#title' => t('Heureka API settings'));
  $form['wrapper']['uc_heureka_api_key_field'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('uc_heureka_api_key_field', ''),
    '#description' => t('API key <a href="!heureka_api_key" target="_blank">Heureka.cz</a>', array('!heureka_api_key' => url('http://sluzby.heureka.cz/sluzby/certifikat-spokojenosti/'))),
  );

  $form['wrapper']['uc_heureka_api_conversion_key_field'] = array(
    '#type' => 'textfield',
    '#title' => t('API conversion key'),
    '#default_value' => variable_get('uc_heureka_api_conversion_key_field', ''),
    '#description' => t('API conversion key <a href="!heureka_api_key" target="_blank">Heureka.cz</a>', array('!heureka_api_key' => url('http://sluzby.heureka.cz/obchody/mereni-konverzi/'))),
  );

  return system_settings_form($form);
}

/**
 * Form validation handler for uc_heureka_settings_form().
 *
 * @see system_settings_form_submit()
 */
function uc_heureka_settings_form_validate($form, &$form_state) {
  // Check API key.
  if (!preg_match('~^[a-zA-Z0-9]+$~', $form_state['values']['uc_heureka_api_key_field'])) {
    form_set_error('uc_heureka_api_key_field', t('Please fill correct API key!'));
  }
  // Check API JS conversion key.
  if (!preg_match('~^[a-zA-Z0-9]+$~', $form_state['values']['uc_heureka_api_conversion_key_field'])) {
    form_set_error('uc_heureka_api_conversion_key_field', t('Please fill correct API conversion key!'));
  }
}
