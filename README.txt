
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

uc_heureka is an add-on module for the Ubercart e-commerce suite. 
The Drupal 5 version is unsupported. 
The Drupal 6 version has been tested with Ubercart 2.11.

This module is providing interface for Heureka services. 
Especially javascript conversion code and service called 
verified by customers.

INSTALLATION
-------------
After enabling the uc_heureka module you will be able to 

1. go to Administer->Store Administration->Configuration->Heureka settings
   in order to set up these services

2. verified by customers service uses Drupal log [yoursite]/admin/reports/dblog. 
   So you can find all sended orders. The type of log is called "heureka". 
   We rocomended set the filter with "heureka" type.
